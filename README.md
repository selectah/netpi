# NETPI

Node.js, etherium, typeorm, postgres, inversify sample application.

## System requirements
- nodejs
- postgres

## Setup
```
npm install -g truffle ganache-cli

git clone https://selectah@bitbucket.org/selectah/netpi.git
cd netpi

npm install
```

## Configuration

```
echo NODE_ENV=development > .env
echo EXPRESS_PORT=3000  >> .env
echo EXPRESS_HOST=localhost >> .env
echo DB_TYPE=postgres >> .env
echo DB_HOST=localhost >> .env
echo DB_PORT=5432 >> .env 
echo DB_USERNAME=postgres >> .env
echo DB_PASSWORD=postgres >> .env
echo DB_NAME=ethwallets >> .env
echo BLOCKCHAIN_URL=ws://localhost:8545 >> .env
```

#### Run Ganache test blockchain node
```
ganache-cli --db runtime/ganache --acctKeys runtime/ganache/keys.json -u 0 --networkId 777 --mnemonic "<mnemonic>"
```

#### Run Express server
```
npm run build && npm start
```

## API

### Transfer funds 

#### Request
**Method:** POST

**URL:** /api/transfers

**Content-Type:** application/json

**Body:**
```
{
    "from": "0x2890a84c32262bae44964d17d3c3fa4c0be01316",
    "to": "0x95538e2f365a78ec99e38f4cb170950742c14972",
    "amount": 100
}
```

#### Response


**Status:** 200

**Content-Type:** application/json

**Body:**
```
{
    "from": "0x2890a84c32262bae44964d17d3c3fa4c0be01316",
    "to": "0x95538e2f365a78ec99e38f4cb170950742c14972",
    "amount": 100,
    "state": 3,
    "id": 1
}
```

**Status:** 400
**Content-Type:** application/json

**Body:**
```
{
    "errors": [
        {
            "value": "asdf",
            "msg": "Not a numeric value",
            "param": "amount",
            "location": "body"
        }
    ]
}
```

**Status:** 404

**Content-Type:** text/plain

**Body:**
```
Not Found
```

**Status:** 500

**Content-Type:** text/plain

**Body:**
```
Internal server error
```

### Get addresses

#### Request

**Method:** GET

**URL:** /api/accounts

#### Response

**Content-Type:** application/json

**Body:**
```
[
    "0x7D5eFDb0A83080aC0FC13684a222858855861431",
    "0x2890a84c32262BAE44964d17d3C3fa4C0BE01316",
    "0x95538E2F365a78eC99E38f4CB170950742c14972",
    "0x319FA97Fd6000A596Ae8b7dB5Bb0cA7bdF158c24",
    "0x9091B9d16bd8D5CD3241e1994F006ADCC20035ba",
    "0xA0798839378ab0D3d25Ad4fa1B4c05B1cA41a500",
    "0x5ec994FA270B3A29Eb578c46DEE4BC405c2A1d08",
    "0xB00BB63f48EB366917bB6a224c69551Ef8495957",
    "0x8e4F0391Bc8aC81347388020cad7B759C445FAaC",
    "0xd9F39e5c37B826Aa6980350c45388B2094F59a60"
] 
```

### Get account balance

#### Request

**Method:** GET

**URL:** /api/accounts/**:address**/balance   

#### Response

**Content-Type:** text/plain

**Body:**
```
100000000000000002000
```