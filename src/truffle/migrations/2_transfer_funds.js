const TransferFunds = artifacts.require("TransferFunds");
const fs = require('fs');
const contractData = require('../build/contracts/TransferFunds');
module.exports = function (deployer, network, accounts) {
  deployer.deploy(TransferFunds, accounts[0]).then(() => {
    const deployedData = {
      abi: contractData.abi,
      bytecode: contractData.bytecode,
      address: TransferFunds.address
    };
    fs.writeFileSync(__dirname + '/../build/contracts/TransferFundsDeployed.json',
      JSON.stringify(deployedData, null, '\t'),
      {encoding: 'utf8'}
    );
  });
};
