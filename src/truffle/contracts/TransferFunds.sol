// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract TransferFunds {
    address payable owner;
    address payable sender;
    address payable receiver;
    mapping(address => uint) public balances;

    constructor(address payable ownerAddress) {
        owner = ownerAddress;
    }

    function getBalance() public view returns (uint256) {
        return address(this).balance;
    }

    function deposit() public payable {}

    function transfer(address payable receiver, uint amount, uint commission) public payable returns(uint256) {
        uint total = amount + commission;
        address payable _sender = payable(msg.sender);
        if (receiver.send(amount)) {
            if (!owner.send(commission)) {
                revert();
            }
        } else {
            _sender.transfer(address(this).balance);
        }
        return address(this).balance;
    }
}
