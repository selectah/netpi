import 'reflect-metadata';
import log4js, {Logger} from 'log4js';
import {Container} from 'inversify';
import {InversifyExpressServer} from 'inversify-express-utils';
import {Application, Request, Response, NextFunction} from 'express';
import cors from 'cors';
import * as bodyParser from 'body-parser';
import './controllers/WalletsController';
import './controllers/TransfersController';
import './controllers/AccountsController';
import {Types} from './shared/constants/Types';
import {ConfigurationService} from './shared/services/ConfigurationService';
import {DatabaseService} from './shared/services/DatabaseService';
import {WalletService} from './shared/services/WalletService';
import {TransferService} from './shared/services/TransferService';
import {Web3Service} from './shared/services/Web3Service';

(async () => {

    const container = new Container();

    container.bind<ConfigurationService>(Types.ConfigurationService).to(ConfigurationService).inSingletonScope();
    const configurationService = container.get<ConfigurationService>(Types.ConfigurationService);

    log4js.configure(configurationService.logger);
    const logger = log4js.getLogger('log');
    container.bind<Logger>(Types.Logger).toConstantValue(logger);

    container.bind<DatabaseService>(Types.DatabaseService).to(DatabaseService).inSingletonScope();
    container.bind<WalletService>(Types.WalletService).to(WalletService).inSingletonScope();
    container.bind<TransferService>(Types.TransferService).to(TransferService).inSingletonScope();
    container.bind<Web3Service>(Types.Web3Service).to(Web3Service).inSingletonScope();

    const database = container.get<DatabaseService>(Types.DatabaseService);
    await database.connect();
    await database.connection.synchronize();

    const walletService = container.get<WalletService>(Types.WalletService);
    await walletService.initialize();

    const server: InversifyExpressServer = new InversifyExpressServer(container);
    server.setConfig((app: Application) => {
        // set cors
        if (configurationService.environment === 'development') {
            app.use(cors());
        }
        app.use(bodyParser.urlencoded({
            extended: true
        }));
        app.use(bodyParser.json());
        app.use(log4js.connectLogger(logger, {}));

        app.use((error: any, request: Request, response: Response, next: NextFunction) => {
            logger.error(error);
            response.sendStatus(500);
        });

    });

    server.setErrorConfig((app) => {
        app.use((error: any, request: Request, response: Response, next: NextFunction) => {
            logger.error(error.stack);
            response.sendStatus(500);
        });

        app.use((request, response, next) => {
            response.sendStatus(404);
        });
    });

    const instance = server.build();
    instance.listen(configurationService.server.port);
    logger.info('Express server is listening on port ' + configurationService.server.port);

    process.on('unhandledRejection', (error: Error) => {
        if (!error) {
            logger.warn('No reason for the failure, please investigate all the potential possibilities that is causing the error');
        } else {
            logger.error(error);
        }
    });
})();


