export const Types = {
    Logger: Symbol.for('Logger'),
    ConfigurationService: Symbol.for('ConfigurationService'),
    DatabaseService: Symbol.for('DatabaseService'),
    WalletService: Symbol.for('WalletService'),
  TransferService: Symbol.for('TransferService'),
    Web3Service: Symbol.for('Web3Service'),
};