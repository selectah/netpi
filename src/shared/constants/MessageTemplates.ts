export const MessageTemplates = {
    // Request validation
    required: 'Required field',
    minLength: 'Minimum length is %s characters',
    notEthereumAddress: 'Not an Ethereum address',
    notNumeric: 'Not a numeric value',

    // Repo
    entityNotFound: 'Failed to load %s with params %s'
};