import fs from 'fs';
import {Wallet} from '../../entities/Wallet';

interface IKey {
  type: string;
  data: number[];
}

interface IAccount {
  nonce: string;
  balance: string;
  stateRoot: string;
  codeHash: string;
}

interface IAddress {
  secretKey: IKey;
  publicKey: IKey;
  address: string;
  account: IAccount;
}

interface IAddresses {
  addresses: {
    [index: string]: IAddress;
  };
  private_keys: {
    [index: string]: string;
  }
}

export const getInitialWallets = () => {

  const keysJson: string = fs.readFileSync(__dirname + '/../../../runtime/ganache/keys.json', {encoding: 'utf8'});
  const parsedKeys: IAddresses = JSON.parse(keysJson);
  const wallets: Wallet[] = [];
  for (const key in parsedKeys.addresses) {
    const data = parsedKeys.addresses[key];
    const wallet = new Wallet();
    wallet.address = data.address;
    wallet.publicKey = '0x' + Buffer.from(data.publicKey.data).toString('hex');
    wallet.privateKey = '0x' + parsedKeys.private_keys[key];
    wallets.push(wallet);
  }
  return wallets;
};