import {inject, injectable} from "inversify";
import {Connection, ConnectionOptions, createConnection} from 'typeorm';
import {ConfigurationService} from "./ConfigurationService";
import {Types} from "../constants/Types";
import {Wallet} from "../../entities/Wallet";
import {Transfer} from '../../entities/Transfer';

@injectable()
export class DatabaseService {
    private _connectionOptions: ConnectionOptions;
    private _connection!: Connection;

    constructor(@inject(Types.ConfigurationService) private _config: ConfigurationService) {
        this._connectionOptions = {
            ...this._config.database as ConnectionOptions,
          entities: [Wallet, Transfer]
        };
    }

    async connect() {
        this._connection = await createConnection(this._connectionOptions);
    }

    get connection() {
        return this._connection;
    }
}