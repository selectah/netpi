import {inject, injectable} from 'inversify';
import {Wallet} from '../../entities/Wallet';
import {Types} from '../constants/Types';
import {DatabaseService} from './DatabaseService';
import {Repository} from 'typeorm';
import {getInitialWallets} from '../constants/Addresses';

@injectable()
export class WalletService {
  private readonly _repository: Repository<Wallet>;
  private _ownerAddress!: string;

  constructor(
    @inject(Types.DatabaseService) private readonly dbService: DatabaseService) {
    this._repository = dbService.connection.getRepository(Wallet);
  }

  async getAll(): Promise<Wallet[]> {
    const wallets = await this._repository.find();
    return wallets;
  }

  get ownerAddress(): string {
    return this._ownerAddress;
  }

  async initialize() {
    const wallets = await this._repository.find({owner: true});
    if (wallets.length === 0) {
      const wallets = getInitialWallets();
      wallets[0].owner = true;
      this._ownerAddress = wallets[0].address;
      await this.dbService.connection.transaction(async transactionalEntityManager => {
        return await transactionalEntityManager.save<Wallet>(wallets);
      });
    } else {
      this._ownerAddress = wallets[0].address;
    }
  }
}