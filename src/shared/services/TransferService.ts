import {inject, injectable} from 'inversify';
import {Types} from '../constants/Types';
import {DatabaseService} from './DatabaseService';
import {Repository} from 'typeorm';
import {ITransfer, Transfer, TransferState} from '../../entities/Transfer';
import {Web3Service} from './Web3Service';
import {Wallet} from '../../entities/Wallet';
import {ConfigurationService} from './ConfigurationService';
import {Logger} from 'log4js';
import {WalletService} from './WalletService';

@injectable()
export class TransferService {
  private readonly _transferRepo: Repository<Transfer>;
  private readonly _walletRepo: Repository<Wallet>;

  constructor(
    @inject(Types.ConfigurationService) private readonly configurationService: ConfigurationService,
    @inject(Types.DatabaseService) private readonly dbService: DatabaseService,
    @inject(Types.Web3Service) private readonly web3Service: Web3Service,
    @inject(Types.Logger) private readonly logger: Logger
  ) {
    this._transferRepo = dbService.connection.getRepository(Transfer);
    this._walletRepo = dbService.connection.getRepository(Wallet);
  }

  async getAll(): Promise<Transfer[]> {
    const transfers = await this._transferRepo.find();
    return transfers;
  }

  private async create(data: ITransfer): Promise<Transfer> {
    const transfer = new Transfer();
    transfer.from = data.from;
    transfer.to = data.to;
    transfer.amount = data.amount;
    transfer.state = TransferState.created;
    return await this._transferRepo.save(transfer);
  }

  private async deposit(transfer: Transfer, commissionAmount: number): Promise<Transfer> {
      await this.web3Service.contract.methods.deposit()
        .send({
            from: transfer.from,
            gas: 1500000,
            value: transfer.amount + commissionAmount
        });
      transfer.state = TransferState.deposit;
      return await this._transferRepo.save(transfer);
  }

  async sendTransfer(data: Transfer): Promise<Transfer> {
    const transfer = await this.create(data)
      .then(transfer => {
        const commissionAmount = transfer.getCommissionAmount(this.configurationService.agentCommission);
        return this.deposit(transfer, commissionAmount)
          .then(async receipt => {
           const balance = await this.web3Service.contract.methods.transfer(
              transfer.to,
              transfer.amount,
              commissionAmount)
              .send({
                  from: transfer.from,
                  gas: 1500000,
                  value: transfer.amount
              });
            transfer.state = TransferState.complete;
            return this._transferRepo.save(transfer);
        });
    });
    return transfer;

  }
}