import fs from 'fs';
import Web3 from 'web3';
import {WebsocketProvider} from 'web3-core';
import {Eth} from 'web3-eth';
import {inject, injectable} from 'inversify';
import {Types} from '../constants/Types';
import {ConfigurationService} from './ConfigurationService';
import {Logger} from 'log4js';
import {Contract} from 'web3-eth-contract';
import {WalletService} from './WalletService';


@injectable()
export class Web3Service {
  private readonly _web3: Web3;
  private readonly _wsProvider: WebsocketProvider;
  // private compiledContractPath = __dirname + '/../../truffle/build/contracts/TransferFunds.json';
  private deployedContractPath = __dirname + '/../../truffle/build/contracts/TransferFundsDeployed.json';
  private _contract!: Contract;
  private _contractAddress!: string;

  constructor(
    @inject(Types.ConfigurationService) private _config: ConfigurationService,
    @inject(Types.Logger) private logger: Logger,
    @inject(Types.WalletService) private walletsService: WalletService,
  ) {
    this._web3 = new Web3();
    this._wsProvider = new Web3.providers.WebsocketProvider(_config.blockchainUrl);
    this._web3.setProvider(this._wsProvider);
    this.initContract();
  }

  get web3(): Web3 {
    return this._web3;
  }

  get eth(): Eth {
    return this._web3.eth;
  }

  get contract(): Contract {
    return this._contract;
  }

  get contractAddress(): string {
    return this._contractAddress;
  }


  async getBalance(address: string): Promise<string> {
    const balance = await this.eth.getBalance(address);
    // return this.web3.utils.fromWei(balance, 'ether');
    return balance;
  }

  async getOwnerBalance(): Promise<string> {
    const balance = await this.getBalance(this.walletsService.ownerAddress);
    // return this.web3.utils.fromWei(balance, 'ether');
    return balance;
  }

  async getAccounts(): Promise<string[]> {
    return await this.eth.getAccounts();
  }

  private initContract() {
    try {
      const deployedContractSource = fs.readFileSync(this.deployedContractPath, {encoding: 'utf8'});
      const deployedContract = JSON.parse(deployedContractSource);
      this._contract = new this.eth.Contract(deployedContract.abi, deployedContract.address);
      this._contractAddress = deployedContract.address;
    } catch (error) {
      this.logger.fatal('Missed deployed contract data. Please run "npm run contracts:migrate"');
      process.exit();
    }
  }

  /*private deployContract() {
    const compiledContract = JSON.parse(fs.readFileSync(this.compiledContractPath, {encoding: 'utf8'}));
    const contract = new this.eth.Contract(compiledContract.abi);
    contract.deploy({data: compiledContract.bytecode, arguments: [this.ownerAddress]})
      .send({
        from: this.ownerAddress,
        gas: 1500000,
        gasPrice: '10000000000000'
      })
      .then(contractInstance => {
        this.logger.info('Contract is deployed by:  ' + this.ownerAddress);
        this._contract = contractInstance;
        const deployedContract = Object.assign({address: contractInstance.options.address}, compiledContract);
        fs.writeFileSync(this.deployedContractPath, JSON.stringify(deployedContract, null, '\t'));
      })
      .catch((error: Error) => {
        this.logger.fatal('Failed to deploy contract', error.toString());
        process.exit();
      });
  }*/
}