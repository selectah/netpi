import dotenv from 'dotenv';
import {injectable} from 'inversify';
import {Configuration} from 'log4js';

export interface IServerConfiguration {
  host: string;
  port: number;
}

export interface IDatabaseConfiguration {
  type: string;
  host: string;
  port: number;
  database: string;
  username: string;
  password: string;
}

export interface IConfigurationService {
  environment: string;
  server: IServerConfiguration;
  database: IDatabaseConfiguration;
  blockchainUrl: string;
  agentCommission: number;
}

@injectable()
export class ConfigurationService implements IConfigurationService {
  private _environment!: string;
  private _server!: IServerConfiguration;
  private _database!: IDatabaseConfiguration;
  private _blockchainUrl!: string;
  private _logger!: Configuration;
  private _agentCommission!: number;

  constructor() {
    const dotenvConfig = dotenv.config();
    if (dotenvConfig.error) {
      throw dotenvConfig.error.toString();
    } else if (dotenvConfig.parsed) {
      this._environment = dotenvConfig.parsed.NODE_ENV;
      this._blockchainUrl = dotenvConfig.parsed.BLOCKCHAIN_URL;
      this._agentCommission = Number(dotenvConfig.parsed.AGENT_COMMISSION);
      this.setDBConfig(dotenvConfig.parsed);
      this.setWebServerConfig(dotenvConfig.parsed);
      this.setLoggerConfig();
    }
  }

  get logger(): Configuration {
    return this._logger;
  }

  get database(): IDatabaseConfiguration {
    return this._database;
  }

  get environment(): string {
    return this._environment;
  }

  get server(): IServerConfiguration {
    return this._server;
  }

  get blockchainUrl(): string {
    return this._blockchainUrl;
  }

  get agentCommission(): number {
    return this._agentCommission;
  }

  private setDBConfig(dotenvConfig: any) {
    this._database = {
      type: dotenvConfig.DB_TYPE,
      host: dotenvConfig.DB_HOST,
      port: Number(dotenvConfig.DB_PORT),
      database: dotenvConfig.DB_NAME,
      username: dotenvConfig.DB_USERNAME,
      password: dotenvConfig.DB_PASSWORD
    };
  }

  private setWebServerConfig(dotenvConfig: any) {
    this._server = {
      host: dotenvConfig.EXPRESS_HOST,
      port: Number(dotenvConfig.EXPRESS_PORT)
    };
  }

  private setLoggerConfig() {
    this._logger = this.environment === 'development' ?
      {
        appenders: {log: {type: 'console'}},
        categories: {
          default: {appenders: ['log'], level: 'debug'},
          log: {appenders: ['log'], level: 'debug'}
        }
      } :
      {
        appenders: {
          log: {type: 'file', filename: 'runtime/logs/application.log'},
        },
        categories: {
          default: {appenders: ['log'], level: 'error'},
          log: {appenders: ['log'], level: 'error'}
        }
      };
  }

}
