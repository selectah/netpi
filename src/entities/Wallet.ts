import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

export interface IWallet {
    id: number;
    address: string;
    privateKey: string;
    publicKey: string;
    owner: boolean;
}

@Entity()
export class Wallet implements IWallet {

    @PrimaryGeneratedColumn('increment')
    id!: number;

    @Column({nullable: false})
    address!: string;

    @Column({nullable: false})
    privateKey!: string;

    @Column({nullable: false})
    publicKey!: string;

    @Column({nullable: false, default: false})
    owner!: boolean;
}