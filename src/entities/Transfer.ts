import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

export enum TransferState {
  created,
  deposit,
  failed,
  complete,
}

export interface ITransfer {
  id: number;
  from: string;
  to: string;
  amount: number;
  state: TransferState,
}

@Entity()
export class Transfer implements ITransfer {

  @PrimaryGeneratedColumn('increment')
  id!: number;

  @Column({nullable: false})
  from!: string;

  @Column({nullable: false})
  to!: string;

  @Column('bigint', {nullable: false})
  amount!: number;

  @Column({nullable: false})
  state!: TransferState;

  getCommissionAmount(commission: number): number {
    return Number(this.amount) * commission;
  }
}