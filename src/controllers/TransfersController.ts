import {NextFunction, Request, Response} from 'express';
import {controller, httpGet, httpPost, next, request, response} from 'inversify-express-utils';
import {Types} from "../shared/constants/Types";
import {inject} from "inversify";
import {TransferService} from '../shared/services/TransferService';
import {body} from 'express-validator';
import {MessageTemplates} from '../shared/constants/MessageTemplates';
import validateRequest from '../shared/middleware/ValidateRequest'


const validationRules = {
    send: [
        body('from').isEthereumAddress().withMessage(MessageTemplates.notEthereumAddress),
        body('to').isEthereumAddress().withMessage(MessageTemplates.notEthereumAddress),
        body('amount').isNumeric().withMessage(MessageTemplates.notNumeric),
    ]
};

@controller('/api/transfers')
export class TransfersController {
    constructor(@inject(Types.TransferService) private transferService: TransferService) {

    }

    @httpGet('/')
    public async getTransfers(
        @response() response: Response,
        @next() next: NextFunction
    ) {
        try {
            const transfers = await this.transferService.getAll();
            response.send(transfers);
        } catch (error) {
            response.sendStatus(500);
        }
    }

    @httpPost('/', validateRequest(validationRules.send))
    public async createTransfer(
        @request() request: Request,
        @response() response: Response,
        @next() next: NextFunction
    ) {
        try {
            const transfer = await this.transferService.sendTransfer(request.body);
            response.send(transfer);
        } catch (error) {
            next(error);
        }
    }
}
