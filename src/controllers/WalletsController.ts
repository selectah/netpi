import {NextFunction, Request, Response} from 'express';
import {controller, httpGet, httpPost, next, request, response} from 'inversify-express-utils';
import {Types} from "../shared/constants/Types";
import {inject} from "inversify";
import {WalletService} from "../shared/services/WalletService";

@controller('/api/wallets')
export class WalletsController {
    constructor(@inject(Types.WalletService) private walletService: WalletService) {

    }

    @httpGet('/')
    public async getWallets(
        @response() response: Response,
        @next() next: NextFunction
    ) {
        try {
            const wallets = await this.walletService.getAll();
            response.send(wallets);
        } catch (error) {
            next(error);
        }
    }
}
