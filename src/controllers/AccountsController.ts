import {NextFunction, Request, Response} from 'express';
import {controller, httpGet, next, request, response} from 'inversify-express-utils';
import {Types} from "../shared/constants/Types";
import {inject} from "inversify";
import {Web3Service} from '../shared/services/Web3Service';

@controller('/api/accounts')
export class AccountsController {
    constructor(@inject(Types.Web3Service) private web3Service: Web3Service) {

    }

    @httpGet('/')
    public async getAccounts(
        @response() response: Response,
        @next() next: NextFunction
    ) {
        try {
            const accounts = await this.web3Service.getAccounts();
            response.send(accounts);
        } catch (error) {
            next(error);
        }
    }

  @httpGet('/contract/balance')
  public async getContractBalance(
    @response() response: Response,
    @next() next: NextFunction
  ) {
    try {
      const balance = await this.web3Service.getBalance(this.web3Service.contractAddress);
      response.send(balance);
    } catch (error) {
      next(error);
    }
  }

  @httpGet('/owner/balance')
  public async getOwnerBalance(
    @response() response: Response,
    @next() next: NextFunction
  ) {
    try {
      const balance = await this.web3Service.getOwnerBalance();
      response.send(balance);
    } catch (error) {
      next(error);
    }
  }

  @httpGet('/:address/balance')
  public async getAccountBalance(
    @request() request: Request,
    @response() response: Response,
    @next() next: NextFunction
  ) {
    try {
      const balance = await this.web3Service.getBalance(request.params.address);
      response.send(balance);
    } catch (error) {
      next(error);
    }
  }
}
